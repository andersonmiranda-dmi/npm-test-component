# Getting Started

This is a small project for testing the Artbasel UI kit.

### Installing

Install dependencies.

`yarn install`

### Running the app

`yarn start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

# How to use artbasel-ui-kit-test in your project

### Install artbasel-ui-kit-test

`yarn add artbasel-ui-kit-test`

or

`npm install artbasel-ui-kit-test`

### Copy assets to your project

To the fonts from the ui-kit to be accessible from yor React app, you need to copy the assets from the package to your project

`cp -R node_modules/artbasel-ui-kit-test/dist/assets ./src/`

### Import styles and components

Add styles.css to your app, by importing it in the index.js file

`import 'artbasel-ui-kit-test/dist/styles.css';`

Import the components in your page and use it as React component.

`import { PrimaryButton } from 'artbasel-ui-kit-test';`

### Usage example

Please ref to App.js to see more details.


### Global variables

We have global variables with colors and other global definitions to be used on your app, if necessary, in SASS and less format:

`artbasel-ui-kit-test/dist/global-variables.scss`

and

`artbasel-ui-kit-test/dist/global-variables.less`

App.moduless.scss is an example on how to use the global variables.
