import React from 'react';
import {
  PrimaryButton,
  SecondaryButton,
  BackButton,
  CloseButton,
  Grid,
  H1,
  Icon,
} from 'artbasel-ui-kit-test';

import './App.modules.scss';

function App() {
  return (
    <>
      <H1>npm library test</H1>

      <Grid celled="internally">
        <Grid.Row>
          <Grid.Column width={2}>Primary Button</Grid.Column>
          <Grid.Column width={10}>
            <PrimaryButton>This is a primary button</PrimaryButton>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>Secondary Button</Grid.Column>
          <Grid.Column width={10}>
            <SecondaryButton>Secondary</SecondaryButton>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>Back Button</Grid.Column>
          <Grid.Column>
            <BackButton width={10}> Back button</BackButton>
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>Close Button</Grid.Column>
          <Grid.Column width={10}>
            <CloseButton />
          </Grid.Column>
        </Grid.Row>
        <Grid.Row>
          <Grid.Column>Icons</Grid.Column>
          <Grid.Column width={10}>
            <Icon name="add" />
            <Icon name="certificate" />
            <Icon name="hotel" />
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </>
  );
}

export default App;
